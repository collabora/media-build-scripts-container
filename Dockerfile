FROM debian:trixie-slim

RUN apt-get update && apt-get install -y --no-install-recommends git gcc-multilib g++-multilib \
  libgmp-dev libmpfr-dev libmpc-dev make texinfo bison flex gettext dwarves abi-dumper libssl-dev \
  libelf-dev libsqlite3-dev python3-sphinx-rtd-theme dvipng meson libasound2-dev libjpeg-dev \
  libudev-dev pkg-config libjson-c-dev libqt5opengl5-dev libsdl2-dev libbpf-dev clang doxygen \
  emscripten rsync llvm clang libgtk-3-dev wget qemu-system-x86 python3-argcomplete bc virtme-ng \
  xz-utils clang-format clang-tidy clang-tools clang clangd libc++-dev libc++1 libc++abi-dev \
  libc++abi1 libclang-dev libclang1 liblldb-dev libllvm-ocaml-dev libomp-dev libomp5 lld lldb \
  llvm-dev llvm-runtime llvm python3-clang \
  && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN git clone https://gitlab.collabora.com/linux/build-scripts.git --branch changes_to_build_sh

WORKDIR /build-scripts
# FIXME Workaround to funcs.sh requiring environment too early
RUN touch env.sh
RUN ./cross.sh setup
RUN ./cross.sh all
RUN ./prepare_build.sh
RUN git config --global --add safe.directory /build-scripts/media-git
