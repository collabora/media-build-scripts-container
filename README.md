# Container for test procedures of the Linux Kernel V4L2 subsystem

These scripts do all of the common checks, that the maintainer of the subsystem performs, to ensure that no regressions or warnings or style issues are introduced to the codebase.
This container is provided to easy the setup of the tool and to enable running them on non-debian distros, where these routines, at the point of creating this, are not tested yet.

## How it works.

The container contains all of the dependencies for cross compilation and the different tools for testing. What it doesn't contain is the kernel sources in order to keep the image small and to keep latest changes.

### Building a container image.

You can build a new version of the container, with:
```
./container.sh build
```
This will prepare a new docker image and pull the kernel base tree into the `kernel` folder.

Otherwise you can also use a pre-built version of the image, with:
```
TBD
```

And prepare the kernel folder locally with:
```
./prepare_kernel.sh
```

### Running the tests

Once you have the image, you need to figure out the tag of the image (e.g. `docker images`).

#### Preparation

The following command assumes that you have prepared the kernel tree at the `kernel` folder in the same directory as the `container.sh` script (`./prepare_kernel.sh`) and that you have the `env.sh` file stored within a `custom_data` folder in the same directory.
The `env.sh` file is either created automatically with `./container.sh build` or manually with:
```
mkdir -p custom_data
cat > custom_data << EOF
myrepo=git://linuxtv.org/hverkuil/media_tree.git
name="Example Name"
email="example@mail.com"
EOF
```

Change the entries of the `env.sh` file accordingly to match the repository you want to test.

#### Starting the container

Now you can use that tag to enter the container:
```
./container.sh run [TAG]
```

#### Running the tests

Within the container, if this is the first ever run, you will have to add the necessary remotes to the repository with:
```
./build.sh setup
```

Afterwards you can run multiple different tests.
Let us take the following example:
You want to perform all the basic tests for all architectures on a branch called `my_branch` found in the repository `example_repo` stored in the parameter: `my_repo` in the `env.sh` file.

```
./build.sh -test all my_branch
```

You can also perform regression testing with:
```
./build.sh -virtme all my_branch
```
Or if you already built the kernel:
```
./build.sh -virtme-quick all my_branch
```

#### Updating the kernel sources

To include your latest changes into the tree you can simply execute:
```
git remote update
```
Within the media-git folder in the container.
